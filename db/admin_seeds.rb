%w{ admin1 admin2 }.each do |name|
  email = "#{name}@test.com"
  admin_user = User.find_or_initialize_by_email email
  if admin_user.new_record?
    admin_user.password = "password"
    admin_user.admin = true
    admin_user.save
  end
  p admin_user
end

