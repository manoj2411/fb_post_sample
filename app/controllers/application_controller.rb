class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :authenticate_user!

  protected
    def after_sign_in_path_for(resource)
      redirect_path_according_to_role
    end

    def redirect_path_according_to_role
      if current_user.admin?
        users_path
      else
        profile_path
      end
    end

    def redirect_according_to_role
      redirect_to redirect_path_according_to_role
    end

end
