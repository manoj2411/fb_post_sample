class PagesController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:home]
  before_filter :verify_and_redirect_authenticate_user

  private
    def verify_and_redirect_authenticate_user
      redirect_according_to_role if current_user
    end
end
