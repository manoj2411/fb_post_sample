class UsersController < ApplicationController

  before_filter :validation_admin, only: [:index, :new, :create]

  # GET /users
  def index
    @users = User.normal_users

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /users/1
  def show
    @user = current_user

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /users/new
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # POST /users
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        format.html { redirect_to [:users], notice: 'User was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def revoke_publish_permission
    current_user.revoke_publish_permission
    redirect_to :back
  end

  def post_on_wall
    current_user.post_on_wall
    redirect_to :back, notice: 'Posted successfully on your wall.'
  end

  protected
    def validation_admin
      redirect_to redirect_path_according_to_role, notice: 'Unauthorized access' unless current_user.admin?
    end
end
