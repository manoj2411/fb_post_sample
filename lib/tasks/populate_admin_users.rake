desc "Create admin users for app"
task :create_admin_users => :environment do
  load Rails.root.join('db', 'admin_seeds.rb')
end
