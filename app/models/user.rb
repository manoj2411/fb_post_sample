# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)
#  admin                  :boolean
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  provider               :string(255)
#  uid                    :string(255)
#  token                  :string(255)
#  publish_permission     :boolean
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, :omniauth_providers => [:facebook]

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :remember_me, :provider, :uid


  scope :normal_users, ->{ where(admin: [nil, false])}

  def self.find_for_facebook_oauth(auth)
    find_or_initialize_by_email(auth.info.email).tap do |user|
      user.password = Devise.friendly_token[0,20] if user.new_record?
      user.provider = auth.provider
      user.uid = auth.uid
      user.token = auth.credentials.token
      user.publish_permission =  FbGraph::User.me(user.token).permissions.include?(:publish_stream)
      user.save
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end


  def revoke_publish_permission
    FbGraph::User.me(token).revoke! :publish_stream
    self.update_attribute :publish_permission, false
  end

  def post_on_wall(text = nil)
    text ||= 'This is test message'
    FbGraph::User.me(token).feed!( message: text)
  end
end
