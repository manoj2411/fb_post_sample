

## Development Setup

### Clone the repo
```
git clone git@bitbucket.org:manoj2411/fb_post_sample.git
```

### Install dependencies (ensure bundler is installed)
```
cd path/to/app
bundle install
```

UPDATE database config (config/database.yml)
```
# make the appropirate changes like username and password
```

Setup DB
```
rake db:create
rake db:migrate
```

Run rake tak to populate admin users
```
rake create_admin_users
```

And you are ready to go :)
